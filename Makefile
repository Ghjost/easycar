SYMFONY = php bin/console
SYMFONYCLI = symfony
NPM = npm
COMPOSER = composer
DOCKER = docker

## —— 🎹 Symfony ———————————————————————————————————————————————————————————————
serve: ## Start Symfony server
	$(SYMFONYCLI) serve

migration: ## Create migration
	$(SYMFONY) make:migration 

migrate: ## Update database with new migration(s)
	$(SYMFONY) d:m:m 

controller: ## Create new controller
	$(SYMFONY) make:controller

entity: ## Create new controller
	$(SYMFONY) make:entity

cc: ## Clear cache
	$(SYMFONY) cache:clear
	
fixtures: ## Create fixtures class
	$(SYMFONY) make:fixtures


## —— 📦 Node ———————————————————————————————————————————————————————————————
watch: ## Start node watch file changer
	$(NPM) run watch
	
build: ## Start node build
	$(NPM) run build


## —— 🖊️  Database ———————————————————————————————————————————————————————————————

init-db: ## Init database
	$(SYMFONY) d:d:d --force --if-exists
	$(SYMFONY) d:d:c
	$(SYMFONY) make:migration --no-interaction
	$(SYMFONY) d:m:m --no-interaction

seed: ## Load fixtures
	$(SYMFONY) doctrine:fixtures:load

seedplus: ## Load fixtures with append
	$(SYMFONY) doctrine:fixtures:load --append


## —— 🐋  Docker ———————————————————————————————————————————————————————————————
docker-build: ## Build Docker image
	$(DOCKER) build . -f ./docker/dockerfile -t poc:easyadmin

docker-run: ## Run image on port 8882
	$(DOCKER) run -p 8882:80 poc:easyadmin


## —— 🛠️  Tools ———————————————————————————————————————————————————————————————
help: ## Show this help
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'


