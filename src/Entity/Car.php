<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\CarRepository;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Vich\UploaderBundle\Mapping\Annotation\Uploadable;
use Vich\UploaderBundle\Mapping\Annotation\UploadableField;

#[ORM\Entity(repositoryClass: CarRepository::class)]
/**
 * @Vich\Uploadable
 */
class Car
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['Brand:collection:read'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['Brand:collection:read'])]
    private $Color;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['Brand:collection:read'])]
    private $Model;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['Brand:collection:read'])]
    private $Plate;

    #[ORM\Column(type: 'date')]
    #[Groups(['Brand:collection:read'])]
    private $PMC;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['Brand:collection:read'])]
    private $CreatedAt;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Groups(['Brand:collection:read'])]
    private $UpdatedAt;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['Brand:collection:read'])]
    private $isPublished;

    #[ORM\Column(type: 'integer')]
    #[Groups(['Brand:collection:read'])]
    private $Door;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['Brand:collection:read'])]
    private $GearBoxType;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['Brand:collection:read'])]
    private $FuelType;

    #[ORM\ManyToOne(targetEntity: CarType::class, inversedBy: 'cars')]
    private $Type;

    #[ORM\ManyToMany(targetEntity: CarOption::class, inversedBy: 'cars')]
    private $Options;

    #[ORM\ManyToOne(targetEntity: CarBrand::class, inversedBy: 'cars')]
    private $Brand;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['Brand:collection:read'])]
    private $Picture;


    // #[Vich\UploadableField(mapping: "car_main_image", fileNameProperty: "Picture")]
    // #[File]
    /**
     * @Vich\UploadableField(mapping="car_main_image", fileNameProperty="Picture")
     * @var File
     */
    private $PictureFile;



    public function __construct()
    {
        $this->Options = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getColor(): ?string
    {
        return $this->Color;
    }

    public function setColor(string $Color): self
    {
        $this->Color = $Color;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->Model;
    }

    public function setModel(string $Model): self
    {
        $this->Model = $Model;

        return $this;
    }

    public function getPlate(): ?string
    {
        return $this->Plate;
    }

    public function setPlate(string $Plate): self
    {
        $this->Plate = $Plate;

        return $this;
    }

    public function getPMC(): ?\DateTimeInterface
    {
        return $this->PMC;
    }

    public function setPMC(\DateTimeInterface $PMC): self
    {
        $this->PMC = $PMC;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->CreatedAt;
    }

    public function setCreatedAt(\DateTimeImmutable $CreatedAt): self
    {
        $this->CreatedAt = $CreatedAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->UpdatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $UpdatedAt): self
    {
        $this->UpdatedAt = $UpdatedAt;

        return $this;
    }

    public function isIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    public function getDoor(): ?int
    {
        return $this->Door;
    }

    public function setDoor(int $Door): self
    {
        $this->Door = $Door;

        return $this;
    }

    public function getGearBoxType(): ?string
    {
        return $this->GearBoxType;
    }

    public function setGearBoxType(string $GearBoxType): self
    {
        $this->GearBoxType = $GearBoxType;

        return $this;
    }

    public function getFuelType(): ?string
    {
        return $this->FuelType;
    }

    public function setFuelType(string $FuelType): self
    {
        $this->FuelType = $FuelType;

        return $this;
    }

    public function getType(): ?CarType
    {
        return $this->Type;
    }

    public function setType(?CarType $Type): self
    {
        $this->Type = $Type;

        return $this;
    }

    /**
     * @return Collection<int, CarOption>
     */
    public function getOptions(): Collection
    {
        return $this->Options;
    }

    public function addOption(CarOption $option): self
    {
        if (!$this->Options->contains($option)) {
            $this->Options[] = $option;
        }

        return $this;
    }

    public function removeOption(CarOption $option): self
    {
        $this->Options->removeElement($option);

        return $this;
    }

    public function getBrand(): ?CarBrand
    {
        return $this->Brand;
    }

    public function setBrand(?CarBrand $Brand): self
    {
        $this->Brand = $Brand;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->Picture;
    }

    public function setPicture(?string $Picture): self
    {
        $this->Picture = $Picture;

        return $this;
    }



    public function setPictureFile(File $Picture = null)
    {
        $this->PictureFile = $Picture;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($Picture) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getPictureFile()
    {
        return $this->PictureFile;
    }
}
