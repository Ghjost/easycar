<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\BrandRepository;
use App\Repository\CarBrandRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Vich\UploaderBundle\Mapping\Annotation\Uploadable;
use Vich\UploaderBundle\Mapping\Annotation\UploadableField;

#[ORM\Entity(repositoryClass: CarBrandRepository::class)]
/**
 * @Vich\Uploadable
 */
class CarBrand
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['Brand:collection:read'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['Brand:collection:read'])]
    private $Brand;

    #[ORM\OneToMany(mappedBy: 'Brand', targetEntity: Car::class)]
    #[Groups(['Brand:collection:read'])]
    private $cars;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['Brand:collection:read'])]
    private $Picture;


    /**
     * Undocumented variable
     *
     * @var DateTimeImmutable $UpdatedAt
     */
    private $UpdatedAt;


    /**
     * @Vich\UploadableField(mapping="brand_main_image", fileNameProperty="Picture")
     * @var File
     */
    #[Groups(['Brand:collection:read'])]
    private $PictureFile;

    public function __construct()
    {
        $this->cars = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBrand(): ?string
    {
        return $this->Brand;
    }

    public function setBrand(string $Brand): self
    {
        $this->Brand = $Brand;

        return $this;
    }

    /**
     * @return Collection<int, Car>
     */
    public function getCars(): Collection
    {
        return $this->cars;
    }

    public function addCar(Car $car): self
    {
        if (!$this->cars->contains($car)) {
            $this->cars[] = $car;
            $car->setBrand($this);
        }

        return $this;
    }

    public function removeCar(Car $car): self
    {
        if ($this->cars->removeElement($car)) {
            // set the owning side to null (unless already changed)
            if ($car->getBrand() === $this) {
                $car->setBrand(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->Brand;
    }

    public function getPicture(): ?string
    {
        return $this->Picture;
    }

    public function setPicture(?string $Picture): self
    {
        $this->Picture = $Picture;

        return $this;
    }

    public function setPictureFile(File $Picture = null)
    {
        $this->PictureFile = $Picture;

        if ($Picture) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->UpdatedAt = new \DateTimeImmutable();
        }
    }

    public function getPictureFile()
    {
        return $this->PictureFile;
    }

    public function setUpdatedAt(\DateTimeImmutable $UpdatedAt): self
    {
        $this->UpdatedAt = $UpdatedAt;

        return $this;
    }
}
