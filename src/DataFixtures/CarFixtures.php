<?php

namespace App\DataFixtures;

use App\Entity\Car;
use Faker\Generator;
use DateTimeImmutable;
use App\Entity\CarType;
use App\Entity\CarBrand;
use App\Entity\CarOption;
use Faker\Provider\Fakecar;
use App\Repository\CarTypeRepository;
use App\Repository\CarBrandRepository;
use App\Repository\CarOptionRepository;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\DBAL\Driver\IBMDB2\Exception\Factory;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;

class CarFixtures extends Fixture implements FixtureGroupInterface
{

    public static function getGroups(): array
    {
        return ['car'];
    }

    const MAIN_PICTURE = array(
        '01G3K3J799B7TC35DF6X0VJCVX.png',
        '01G3KAZ13H09SFJQ7AQN71W09W.jpg',
    );

    public function __construct(
        private CarTypeRepository $carTypeRepository,
        private CarOptionRepository $carOptionRepo,
        private CarBrandRepository $carBrandRepository
    ) {
    }
    public function load(ObjectManager $manager): void
    {

        $faker = (new \Faker\Factory())::create();
        $faker->addProvider(new \Faker\Provider\Fakecar($faker));

        $allTypes = $this->carTypeRepository->findAll();

        $allOptions = $this->carOptionRepo->findAll();
        // dd($allTypes, 'oui', $allOptions);
        $allBrands = $this->carBrandRepository->findAll();

        // dump($allTypes[rand(0, count($allTypes))]);



        for ($i = 0; $i < 20; $i++) {
            $car = new Car();
            $car
                ->setColor($faker->colorName())
                ->setModel($faker->vehicleModel())
                ->setPlate($faker->vehicleRegistration('[A-Z]{2}-[0-9]{3}-[A-Z]{2}'))
                ->setPMC(new DateTimeImmutable())
                ->setCreatedAt(new DateTimeImmutable())
                ->setUpdatedAt(new DateTimeImmutable())
                ->setIsPublished($faker->boolean())
                ->setDoor($faker->vehicleDoorCount)
                ->setGearBoxType(strtoupper($faker->vehicleGearBoxType))
                ->setFuelType(ucfirst($faker->vehicleFuelType))
                ->setType($allTypes[rand(0, count($allTypes) - 1)])
                ->setBrand($allBrands[rand(0, count($allBrands) - 1)])
                // ->setType(($allTypes[rand(1, count($allTypes))]))
                // ->setBrand(($allBrands[rand(1, count($allBrands))]))
                ->setPicture((self::MAIN_PICTURE)[rand(0, 1)]);

            // $randomOption = rand(1, count($allOptions));

            // for ($i = 1; $i < $randomOption; $i++) {
            //     $car->addOption($allOptions[$i]);
            // }

            // rand(0, count(self::CAR_OPTIONS) - 1);

            // for ($i = 0; $i < rand(0, count($allOptions) - 1); $i++) {
            //     $car->addOption($allOptions[rand(0, count($allOptions) - 1)]);
            // }

            $manager->persist($car);
        }

        $manager->flush();
    }
}
