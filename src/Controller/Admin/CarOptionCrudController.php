<?php

namespace App\Controller\Admin;

use App\Entity\CarOption;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class CarOptionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return CarOption::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
