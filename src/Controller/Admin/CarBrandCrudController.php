<?php

namespace App\Controller\Admin;

use App\Entity\CarBrand;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;

class CarBrandCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return CarBrand::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'ID')->hideOnForm(),
            TextField::new('Brand', 'Marque'),
            ImageField::new('Picture', 'Photo')
                ->setUploadDir('public/' . $this->getParameter('vich_path.brand_main_image'))
                ->setUploadedFileNamePattern('[ulid].[extension]')
                ->setBasePath($this->getParameter('vich_path.brand_main_image')),
        ];
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
