<?php

namespace App\Controller\Admin;

use App\Entity\Car;
use App\Entity\CarBrand;
use App\Entity\CarOption;
use App\Entity\CarType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{

    public function __construct(
        private AdminUrlGenerator $adminUrlGenerator
    ) {
    }
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        // return parent::index();

        // return $this->redirect($this->adminUrlGenerator->setController(CarCrudController::class)->generateUrl());
        return $this->redirect($this->adminUrlGenerator->setRoute('app_admin_stats')->generateUrl());

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('EasyadminCar');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToRoute('Statistiques', 'fa fa-home', 'app_admin_stats');
        yield MenuItem::section('Stats', 'fa-solid fa-chart-pie');
        yield MenuItem::subMenu('Voitures', 'fa-solid fa-car')->setSubItems([
            MenuItem::linkToCrud('Liste des voitures', 'fa-solid fa-list', Car::class),
            MenuItem::linkToCrud('Nouvelle voiture', 'fa-solid fa-plus', Car::class)->setAction(Crud::PAGE_NEW),
        ]);
        yield MenuItem::subMenu('Type de voiture', 'fa-solid fa-car')->setSubItems([
            MenuItem::linkToCrud('Liste des types', 'fa-solid fa-list', CarType::class),
            MenuItem::linkToCrud('Nouveau type', 'fa-solid fa-plus', CarType::class)->setAction(Crud::PAGE_NEW),
        ]);
        yield MenuItem::subMenu('Options', 'fa-solid fa-puzzle-piece')->setSubItems([
            MenuItem::linkToCrud('Liste des options', 'fa-solid fa-puzzle-piece', CarOption::class),
            MenuItem::linkToCrud('Nouvelle option', 'fa-solid fa-plus', CarOption::class)->setAction(Crud::PAGE_NEW),
        ]);
        yield MenuItem::subMenu('Marques', 'fa-solid fa-copyright')->setSubItems([
            MenuItem::linkToCrud('Liste des marques', 'fa-solid fa-copyright', CarBrand::class),
            MenuItem::linkToCrud('Nouvelle marque', 'fa-solid fa-plus', CarBrand::class)->setAction(Crud::PAGE_NEW),
        ]);
    }
}
