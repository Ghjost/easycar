<?php

namespace App\Controller;

use App\Controller\Admin\CarCrudController;
use App\Repository\CarBrandRepository;
use App\Repository\CarRepository;
use App\Repository\CarTypeRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CarController extends AbstractController
{
    public function __construct(
        private CarRepository $carRepository,
        private CarBrandRepository $carBrandRepository,
        private CarTypeRepository $carTypeRepository,
        private AdminUrlGenerator $adminUrlGenerator
    ) {
    }

    #[Route('/car/{brand}', name: 'app_car_brand')]
    public function carByBrand(
        string $brand,
    ): Response {

        $carBrand = $this->carBrandRepository->findOneBy(['Brand' => $brand]);
        $car = $this->carRepository->findBy(['Brand' => $carBrand]);

        // dd($car);

        return $this->render('car/by_brand.html.twig', [
            'controller_name' => 'CarController',
            'car_list' => $car,
            'car_brand' => $brand
        ]);
    }

    #[Route('/car/{brand}/{type}', name: 'app_car_brand_type')]
    public function carByBrandType(
        string $brand,
        string $type,
    ): Response {

        $carBrand = $this->carBrandRepository->findOneBy(['Brand' => $brand]);
        $carType = $this->carTypeRepository->findOneBy(['Type' => $type]);
        $car = $this->carRepository->findBy(['Brand' => $carBrand, 'Type' => $carType]);

        // dd($car);

        return $this->render('car/by_brand_type.html.twig', [
            'controller_name' => 'CarController',
            'car_list' => $car,
            'car_brand' => $brand,
            'car_type' => $type
        ]);
    }

    #[Route('/car/{brand}/{type}/{modele}/{id}', name: 'app_car')]
    public function index(
        string $brand,
        string $type,
        string $modele,
        int $id
    ): Response {

        $car = $this->carRepository->findOneBy(['id' => $id]);

        dump($car);
        dump($this->adminUrlGenerator->setController(CarCrudController::class)->setEntityId($car->getId())->generateUrl());

        return $this->render('car/index.html.twig', [
            'controller_name' => 'CarController',
            'car' => $car,
            'edit_url' => $this->adminUrlGenerator
                ->setAction(Crud::PAGE_EDIT)
                ->setController(CarCrudController::class)
                ->setEntityId($car->getId())
                ->generateUrl(),
            'see_url' => $this->adminUrlGenerator
                ->setAction(Crud::PAGE_DETAIL)
                ->setController(CarCrudController::class)
                ->setEntityId($car->getId())
                ->generateUrl(),
        ]);
    }
}
